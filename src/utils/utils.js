export const coinExchange = input => {
  const format = [100, 50, 20, 10, 5, 1, 0.25, 0.1, 0.05, 0.01];
  let amount = "";
  format.map(i => {
    const coins = Math.floor(input / i);
    input -= i * coins;
    input = Math.round(input * 100) / 100;
    if (coins !== 0) {
      const unit = i < 1 ? "" : ` ${i}`;
      amount += `${coins}${unit} ${getCurrentcy(i, coins > 1)} \n`;
    }
  });
  return amount;
};
export const getCurrentcy = (unit, isMany) => {
  let currentcy;
  if (unit >= 1) {
    currentcy = isMany ? "dollar bills" : "dollar bill";
  } else if (unit >= 0.25) {
    currentcy = isMany ? "quarters" : "quarter";
  } else if (unit >= 0.1) {
    currentcy = isMany ? "dimies" : "dime";
  } else if (unit >= 0.05) {
    currentcy = isMany ? "nickels" : "nickel";
  } else if (unit >= 0.01) {
    currentcy = isMany ? "pennies" : "penny";
  }
  return currentcy;
};
