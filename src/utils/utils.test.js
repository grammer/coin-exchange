import { coinExchange, getCurrentcy } from "./utils";

test("coinExchange", () => {
  expect(coinExchange(1)).toBe("1 1 dollar bill \n");
  expect(coinExchange(0.99)).toBe("3 quarters \n2 dimies \n4 pennies \n");
  expect(coinExchange(124.67)).toBe("1 100 dollar bill \n1 20 dollar bill \n4 1 dollar bills \n2 quarters \n1 dime \n1 nickel \n2 pennies \n");
});
// test("100 to 1 dollar currentcy 1 coin ", () => {
//   expect(getCurrentcy(100, false)).toBe("dollar bill");
//   expect(getCurrentcy(80, false)).toBe("dollar bill");
//   expect(getCurrentcy(60, false)).toBe("dollar bill");
//   expect(getCurrentcy(20, false)).toBe("dollar bill");
//   expect(getCurrentcy(1, false)).toBe("dollar bill");
// });
// test("100 to 1 dollar currentcy many coins", () => {
//   expect(getCurrentcy(100, true)).toBe("dollar bills");
//   expect(getCurrentcy(80, true)).toBe("dollar bills");
//   expect(getCurrentcy(60, true)).toBe("dollar bills");
//   expect(getCurrentcy(20, true)).toBe("dollar bills");
//   expect(getCurrentcy(1, true)).toBe("dollar bills");
// });
// test("input 0.99 to 0.25 1 coin", () => {
//   expect(getCurrentcy(0.99, false)).toBe("quarter");
//   expect(getCurrentcy(0.25, false)).toBe("quarter");
// });
// test("input 0.99 to 0.25 many coins", () => {
//   expect(getCurrentcy(0.99, true)).toBe("quarters");
//   expect(getCurrentcy(0.25, true)).toBe("quarters");
// });
// test("input 0.24 to 0.1 1 coin", () => {
//   expect(getCurrentcy(0.24, false)).toBe("dime");
//   expect(getCurrentcy(0.11, false)).toBe("dime");
//   expect(getCurrentcy(0.1, false)).toBe("dime");
// });
// test("input 0.24 to 0.1 many coins", () => {
//   expect(getCurrentcy(0.24, true)).toBe("dimies");
//   expect(getCurrentcy(0.11, true)).toBe("dimies");
//   expect(getCurrentcy(0.1, true)).toBe("dimies");
// });
// test("input 0.09 to 0.05 1 coin", () => {
//   expect(getCurrentcy(0.09, false)).toBe("nickel");
//   expect(getCurrentcy(0.05, false)).toBe("nickel");
// });
// test("input 0.09 to 0.05 many coins", () => {
//   expect(getCurrentcy(0.09, true)).toBe("nickels");
//   expect(getCurrentcy(0.05, true)).toBe("nickels");
// });
// test("input 0.04 to 0.01 1 coin", () => {
//   expect(getCurrentcy(0.04, false)).toBe("penny");
//   expect(getCurrentcy(0.01, false)).toBe("penny");
// });
// test("input 0.04 to 0.01 many coins", () => {
//   expect(getCurrentcy(0.04, true)).toBe("pennies");
//   expect(getCurrentcy(0.01, true)).toBe("pennies");
// });
