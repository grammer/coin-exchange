import React from "react";
import PropTypes from "prop-types";
import { coinExchange } from "./../../utils/utils";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

class FormInput extends React.Component {
  state = {
    dollar: "",
    lists: []
  };
  handleChange = name => event => {
    const val = event.target.value;
    const reg = /^\d{0,9}(\.\d{0,2}){0,1}$/g;
    if (reg.test(val) || val === "") {
      this.setState({ [name]: event.target.value });
    }
    // this.setState({ [name]: val.match( /\d+/g ) });
  };
  handleCalculator = () => event => {
    try {
      const coins = coinExchange(this.state.dollar).split("\n");
      this.setState({ lists: coins });
    } catch (error) {
      console.error(error);
    }
  };
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            label="Dollar Bills"
            margin="normal"
            className={classes.textField}
            value={this.state.dollar}
            onChange={this.handleChange("dollar")}
          />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleCalculator()}
          >
            Calculator
          </Button>
        </form>

        <List component="nav">
          {this.state.lists.length > 0 &&
            this.state.lists.map(i => (
              <ListItem button key={i}>
                <ListItemText primary={i} />
              </ListItem>
            ))}
        </List>
      </React.Fragment>
    );
  }
}

FormInput.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(FormInput);
